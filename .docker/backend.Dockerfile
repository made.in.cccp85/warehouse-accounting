FROM php:8.2.9-fpm-alpine

ENV PHP_MEMORY_LIMIT=512M
ENV PHP_UPLOAD_MAX_FILESIZE: 512M
ENV PHP_POST_MAX_SIZE: 512M

RUN apk update \
    && apk upgrade \
    && apk add --update openssl gnupg ca-certificates zip unzip supervisor libpng-dev oniguruma-dev libxml2-dev libzip-dev icu-dev openldap-dev \
    && docker-php-ext-install pdo_mysql gd mbstring xml zip bcmath soap intl ldap \
    && docker-php-ext-enable pdo_mysql gd mbstring xml zip bcmath soap intl ldap \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apk/*

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
