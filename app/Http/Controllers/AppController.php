<?php

namespace App\Http\Controllers;

use App\Services\TransactionService;
use Inertia\Response;

class AppController extends Controller
{
    public function index(TransactionService $service): Response
    {
        return inertia('Dashboard', [
            'transactions' => $service->history(),
            'rest' => $service->rest(),
        ]);
    }
}
