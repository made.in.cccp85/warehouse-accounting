<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Services\CRUDService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, CRUDService $service): Response
    {
        return inertia('Products/Index', [
            'items' => $service
                ->filter(Product::class, $request->only(['like', 'where', 'order']))
                ->paginate($request->integer('perPage', 20))
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        return inertia('Products/Item');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductRequest $request, CRUDService $service): RedirectResponse
    {
        $validated = $request->validated();
        $service->save(Product::class, $validated);

        return to_route('products.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        return inertia('Products/Item', $product->toArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductRequest $request, Product $product, CRUDService $service): RedirectResponse
    {
        $validated = $request->validated();
        $service->save($product, $validated);

        return to_route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product): RedirectResponse
    {
        if ($product->transactions->count() > 0) {
            return back()->withErrors('Не возможно удалить товар, по нему есть движения!', 'productHasTransactions');
        }
    }
}
