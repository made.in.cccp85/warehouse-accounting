<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductIncomeRequest;
use App\Http\Requests\UpdateProductIncomeRequest;
use App\Models\Product;
use App\Models\ProductIncome;
use App\Models\ProductTransaction;
use App\Services\CRUDService;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inertia\Response;

class ProductIncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, CRUDService $service): Response
    {
        return inertia('ProductIncomes/Index', [
            'items' => $service
                ->filter(ProductIncome::class, $request->only(['like', 'whereDate', 'order']))
                ->orderByDesc('transacted_at')
                ->orderByDesc('updated_at')
                ->withSum('productTransactions as amount', 'amount')
                ->paginate($request->integer('perPage', 20))
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        return inertia('ProductIncomes/Item', [
            'number' => Str::padLeft(ProductIncome::max('id') + 1, 10, '0'),
            'products' => Product::select('id', DB::raw("CONCAT(vendor_code, ' | ', name) as name"))->pluck('name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductIncomeRequest $request, CRUDService $service): RedirectResponse
    {
        $validated = $request->validated();
        $productTransactions = Arr::pull($validated, 'product_transactions');

        $transacted_at = $validated['transacted'] ? $validated['transacted_at'] : null;
        data_set($productTransactions, '*.transacted_at', $transacted_at);
        $service
            ->save(ProductIncome::class, $validated)
            ->productTransactions()
            ->createMany($productTransactions);

        return to_route('product-incomes.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProductIncome $productIncome): Response
    {
        return inertia('ProductIncomes/Item', [
            ...$productIncome->loadMorph('productTransactions', [ProductTransaction::class])->toArray(),
            'products' => Product::select('id', DB::raw("CONCAT(vendor_code, ' | ', name) as name"))->pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductIncomeRequest $request, ProductIncome $productIncome, CRUDService $service): RedirectResponse
    {
        $validated = $request->validated();
        $productTransactions = Arr::pull($validated, 'product_transactions');

        $transacted_at = $validated['transacted'] ? $validated['transacted_at'] : null;
        data_set($productTransactions, '*.transacted_at', $transacted_at);
        DB::beginTransaction();
        $productIncome->productTransactions()->delete();
        $service
            ->save($productIncome, $validated)
            ->productTransactions()
            ->createMany($productTransactions);
        DB::commit();

        return to_route('product-incomes.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProductIncome $productIncome)
    {
        DB::beginTransaction();
        $productIncome->productTransactions()->delete();
        $productIncome->delete();
        DB::commit();

        return to_route('product-incomes.index');
    }
}
