<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductOutgoRequest;
use App\Models\Product;
use App\Models\ProductOutgo;
use App\Models\ProductTransaction;
use App\Services\CRUDService;
use App\Services\TransactionService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inertia\Response;

class ProductOutgoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, CRUDService $service): Response
    {
        return inertia('ProductOutgos/Index', [
            'items' => $service
                ->filter(ProductOutgo::class, $request->only(['like', 'whereDate', 'order']))
                ->orderByDesc('transacted_at')
                ->orderByDesc('updated_at')
                ->withSum('productTransactions as amount', 'amount')
                ->paginate($request->integer('perPage', 20))
                ->withQueryString(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(TransactionService $service): Response
    {
        return inertia('ProductOutgos/Item', [
            'number' => Str::padLeft(ProductOutgo::max('id') + 1, 10, '0'),
            'products' => $service->rest(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductOutgoRequest $request, CRUDService $service): RedirectResponse
    {
        $validated = $request->validated();
        $productTransactions = Arr::pull($validated, 'product_transactions');

        data_set($productTransactions, '*.transacted_at', $validated['transacted_at'] ?? null);
        $service
            ->save(ProductOutgo::class, $validated)
            ->productTransactions()
            ->createMany($productTransactions);

        return to_route('product-outgos.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProductOutgo $productOutgo, TransactionService $service): Response
    {
        return inertia('ProductOutgos/Item', [
            ...$productOutgo->loadMorph('productTransactions', [ProductTransaction::class])->toArray(),
            'products' => $service->rest(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductOutgoRequest $request, ProductOutgo $productOutgo, CRUDService $service): RedirectResponse
    {
        $validated = $request->validated();
        $productTransactions = Arr::pull($validated, 'product_transactions');

        data_set($productTransactions, '*.transacted_at', $validated['transacted_at'] ?? null);
        DB::beginTransaction();
        $productOutgo->productTransactions()->delete();
        $service
            ->save($productOutgo, $validated)
            ->productTransactions()
            ->createMany($productTransactions);
        DB::commit();

        return to_route('product-outgos.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProductOutgo $productOutgo)
    {
        DB::beginTransaction();
        $productOutgo->productTransactions()->delete();
        $productOutgo->delete();
        DB::commit();

        return to_route('product-outgos.index');
    }
}
