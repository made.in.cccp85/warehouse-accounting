<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductIncomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $ruleNumber = Rule::unique('product_incomes');
        return [
            'number'                            => [
                'required',
                'string',
                'max:10',
                $this->id ? $ruleNumber->ignore($this->id) : $ruleNumber
            ],
            'transacted'                        => 'boolean',
            'transacted_at'                     => 'nullable|required_if:transacted,true',
            'product_transactions'              => 'array|min:1',
            'product_transactions.*.product_id' => 'required|distinct',
            'product_transactions.*.count'      => 'required|integer|gt:0',
            'product_transactions.*.price'      => 'required|integer',
            'product_transactions.*.amount'     => 'required|integer',
        ];
    }
}
