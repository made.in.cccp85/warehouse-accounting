<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class ProductIncome extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'transacted',
        'transacted_at',
    ];

    protected $casts = [
        'transacted'    => 'boolean',
        'created_at'    => 'datetime:Y-m-d H:i:s',
        'updated_at'    => 'datetime:Y-m-d H:i:s',
        'transacted_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $attributes = [
        'transacted' => false,
    ];

    public function productTransactions(): MorphMany
    {
        return $this->morphMany(ProductTransaction::class, 'transactable');
    }
}
