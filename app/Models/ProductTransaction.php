<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class ProductTransaction extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'transactable_type',
        'transactable_id',
        'product_id',
        'transacted_at',
        'count',
        'price',
        'amount',
    ];

    public function transactable(): MorphTo
    {
        return $this->morphTo();
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
