<?php

namespace App\Services;

use App\Enums\CRUDRedirects;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;

class CRUDService
{
    private null|Model $model = null;

    public function filter(string $modelClass, array $data = []): Builder
    {
        /** @var Builder $query */
        $query = $modelClass::query();
        foreach (data_get($data, 'like', []) as $field => $value) {
            if (!empty($value)) {
                $query->where($field, 'like', "%$value%");
            }
        }
        foreach (data_get($data, 'where', []) as $field => $value) {
            if (!empty($value)) {
                $query->where($field, $value);
            }
        }
        foreach (data_get($data, 'whereDate', []) as $field => $value) {
            if (!empty($value)) {
                $query->whereDate($field, $value);
            }
        }
        foreach (data_get($data, 'order', []) as $field => $direction) {
            if (!empty($direction)) {
                $query->orderBy($field, $direction);
            }
        }
        return $query;
    }

    public function save(string|Model $model, array $data, null|\Closure $closure = null): Model
    {
        $data = is_null($closure) ? $data : $closure($data);
        if (is_string($model)) {
            $model = $model::create($data);
        }
        else {
            $model->update($data);
        }
        $this->model = $model;

        return $model;
    }
}
