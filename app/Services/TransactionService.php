<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TransactionService
{
    public function rest(): Collection
    {
        return collect(DB::select("
            SELECT * FROM (
                SELECT
                    p.id,
                    p.name,
                    p.vendor_code,
                    CONCAT(RPAD(p.vendor_code, 10, ' '), ' | ', p.name) AS vendor_name,
                    i.price,
                    CASE
                        WHEN ISNULL(o.count) THEN CAST(i.count AS UNSIGNED)
                        WHEN NOT ISNULL(o.count) THEN CAST(i.count - o.count AS UNSIGNED)
                    END AS count,
                    CASE
                        WHEN ISNULL(o.amount) THEN CAST(i.amount AS UNSIGNED)
                        WHEN NOT ISNULL(o.amount) THEN CAST(i.amount - o.amount AS UNSIGNED)
                    END AS amount
                FROM products p
                LEFT JOIN
                    (
                        SELECT product_id, price, SUM(count) AS count, SUM(amount) AS amount FROM product_transactions
                        WHERE transacted_at IS NOT NULL
                            AND transactable_type LIKE '%ProductIncome'
                        GROUP BY product_id, price
                    ) AS i ON p.id = i.product_id
                LEFT JOIN
                    (
                        SELECT product_id, price, SUM(count) AS count, SUM(amount) AS amount FROM product_transactions
                        WHERE transacted_at IS NOT NULL
                            AND transactable_type LIKE '%ProductOutgo'
                        GROUP BY product_id, price
                    ) AS o ON p.id = o.product_id
            ) AS r WHERE r.count IS NOT NULL
            ORDER BY r.name
            ")
        );
    }

    public function history(): Collection
    {
        return DB::table('product_transactions')
            ->whereNotNull('transacted_at')
            ->selectRaw("
                    transactable_id AS id,
                    transacted_at,
                    CAST(SUM(count) AS UNSIGNED) AS count,
                    CAST(SUM(amount) AS UNSIGNED) AS amount,
                    CASE
                        WHEN transactable_type LIKE '%ProductIncome' THEN 'income'
                        WHEN transactable_type LIKE '%ProductOutgo' THEN 'outgo'
                    END AS type
                ")
            ->groupBy(['transactable_id', 'transacted_at', 'transactable_type'])
            ->orderByDesc('transacted_at')
            ->get();
    }
}
