<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_outgos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->timestamp('transacted_at')->nullable();
            $table->string('number', 10)->nullable()->unique();
            $table->boolean('transacted')->default(false);

            $table->index(['transacted_at', 'number', 'transacted']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_outgos');
    }
};
