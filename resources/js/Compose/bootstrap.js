export function attributesNameId(name, id) {
    if (name && id === undefined) {
        id = name;
    }
    if (id && name === undefined) {
        name = id;
    }

    return {name, id};
}
