<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductIncomeController;
use App\Http\Controllers\ProductOutgoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AppController::class, 'index'])
    ->name('index');

Route::resource('products', ProductController::class)
    ->except(['show']);
Route::resource('product-incomes', ProductIncomeController::class)
    ->except(['show']);
Route::resource('product-outgos', ProductOutgoController::class)
    ->except(['show']);
